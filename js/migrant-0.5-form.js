///////////////// Обработчики форм ////////////////

// очистить
$('#clear').click (function ClearFirmData() {
	localStorage.clear()
});

//сменить
$('#changefirmdata').click (function ChangeFirmData() {
	$("input[role='firm']").each(function(elem) {
localStorage.setItem($(this).attr('name'), $(this).val())
	});
});

// Установить / сменить константы
$('#changeconst').click (function ChangeConstData() {
	$("input[role='const']").each(function(elem) {
		localStorage.setItem($(this).attr('name'), $(this).val());
	}),
	$("input[role='const_firmstatus']").each(function(elem) {
		if ($(this).prop("checked")) 
			localStorage.setItem($(this).attr('id'), 'X')
		else
			localStorage.setItem($(this).attr('id'), ' ');
	});
	$("input[role='const_work']").each(function(elem) {
		if ($(this).prop("checked"))
			localStorage.setItem($(this).attr('id'), 'X')
		else
			localStorage.setItem($(this).attr('id'), ' ');
	});
});

// заполнить существующими
$("input[role='firm']").each(function(elem) {
	$(this).val(localStorage[$(this).attr('name')])	
});
$("input[role='const']").each(function(elem) {
	$(this).val(localStorage[$(this).attr('name')])
});
$("input[role='const_firmstatus']").each(function(elem) {
	if (localStorage.getItem($(this).attr('id')) == 'X')
$(this).prop("checked", true);
}),
$("input[role='const_work']").each(function(elem) {
	if (localStorage.getItem($(this).attr('id')) == 'X')
$(this).prop("checked", true);
}),

// Записать данные гражданина и перейти к форме
$('#notification_print').click (function SaveCitizenData() {
	$("input[role='citizen']").each(function(elem) {
		localStorage.setItem($(this).attr('name'), $(this).val())
	});
	if ($("#start_work").prop("checked")) 
		document.location.href = 'startwork.html'
	else if ($("#end_work").prop("checked"))
		document.location.href = 'endwork.html'
	else
	$("div.warning").html("Выберите форму, которую нужно заполнить!");
	$("input[role='citizen_init']").each(function(elem) {
		if ($(this).prop("checked"))
			localStorage.setItem($(this).attr('id'), 'X')
		else
			localStorage.setItem($(this).attr('id'), ' ');
	});
	$("input[role='citizen_sex']").each(function(elem) {
		if ($(this).prop("checked"))
			localStorage.setItem($(this).attr('id'), 'X')
		else
			localStorage.setItem($(this).attr('id'), ' ');
	});
});
// Записать данные гражданина и сохранить в pdf
$('#notification_save').click (function SaveCitizenData() {
	$("input[role='citizen']").each(function(elem) {
		localStorage.setItem($(this).attr('name'), $(this).val())
	});
	localStorage.setItem('save_pdf', '1');
	if ($("#start_work").prop("checked")) 
		document.location.href = 'startwork.html'
	else if ($("#end_work").prop("checked"))
		document.location.href = 'endwork.html'
	else
	$("div.warning").html("Выберите форму, которую нужно заполнить!");
});